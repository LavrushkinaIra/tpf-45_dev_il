import React from 'react'
import { Calculator } from './src/components/Calculator'
import { CalcState } from './src/context/calculator/calcState'
import { ScreenState } from './src/context/screen/screenState'

export default function App() {
	return (
		<ScreenState>
			<CalcState>
				<Calculator />
			</CalcState>
		</ScreenState>
	)
}
