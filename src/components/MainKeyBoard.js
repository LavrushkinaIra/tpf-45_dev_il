import React, { useContext } from 'react'
import { View } from 'react-native'
import { Row } from './ui/Row'
import { Button } from './ui/Button'
import { CalcContext } from '../context/calculator/calcContext'
import { ScreenContext } from '../context/screen/screenContext'

const btnValues = [
	['C', '+/-', '%', '/'],
	[7, 8, 9, 'x'],
	[4, 5, 6, '-'],
	[1, 2, 3, '+'],
	[0, '.', '=']
]

export const MainKeyBoard = () => {
	const { buttonClickHandler } = useContext(CalcContext)
	const { isLandscape, deviceWidth } = useContext(ScreenContext)

	return (
		<View style={{ width: isLandscape ? '40%' : deviceWidth }}>
			{btnValues.map((row, i) => (
				<Row key={i}>
					{row.map((btn, ind) => (
						<Button
							key={ind}
							value={btn}
							handler={() => buttonClickHandler(btn)}
							style={
								btn === 'C' || btn === '+/-' || btn === '%'
									? 'secondary'
									: btn === '/' || btn === 'x' || btn === '-' || btn === '+' || btn === '='
									? 'accent'
									: btn === 0
									? 'double'
									: ''
							}
						/>
					))}
				</Row>
			))}
		</View>
	)
}
