import React from 'react'
import { View } from 'react-native'
import { Row } from './ui/Row'
import { Button } from './ui/Button'

const btnValues = [
	['(', ')', 'mc', 'm+', 'm-', 'mr'],
	['2^nd', 'x^2', 'x^3', 'x^y', 'e^x', '10^x'],
	['1/x', '√(2&x)', '√(3&x)', '√(4&x)', 'In', '〖log〗_10'],
	['x!', 'sin', 'cos', 'tan', 'e', 'EE'],
	['Rad', 'sinh', 'cosh', 'tanh', 'π', 'Rand']
]

export const AdditionalKeyBoard = () => {
	return (
		<View style={{ width: '60%' }}>
			{btnValues.map((row, i) => (
				<Row key={i}>
					{row.map((btn, ind) => (
						<Button key={ind} value={btn} style={'additional'} />
					))}
				</Row>
			))}
		</View>
	)
}
