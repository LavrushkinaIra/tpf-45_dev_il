import React, { useContext, useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View, SafeAreaView, Text, Dimensions } from 'react-native'
import { CommonKeyBord } from './CommonKeyBord'
import { MainKeyBoard } from './MainKeyBoard'
import { THERME } from '../therme'
import { CalcContext } from '../context/calculator/calcContext'
import { ScreenContext } from '../context/screen/screenContext'

export const Calculator = () => {
	const { number, result } = useContext(CalcContext)

	const { changeScreen, isLandscape } = useContext(ScreenContext)

	useEffect(() => {
		const change = Dimensions.addEventListener('change', ({ window }) => {
			changeScreen(window.width)
		})
		return () => change?.remove()
	})

	return (
		<View style={styles.container}>
			<StatusBar style="light" />
			<SafeAreaView>
				<View>
					<Text style={styles.computedValue}>{number ? number : result}</Text>
				</View>
				{isLandscape ? <CommonKeyBord /> : <MainKeyBoard />}
			</SafeAreaView>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: THERME.TXT_SECONDARY,
		justifyContent: 'flex-end',
		paddingBottom: 40
	},
	computedValue: {
		color: THERME.TXT_WHITE,
		fontSize: 60,
		textAlign: 'right'
	}
})
