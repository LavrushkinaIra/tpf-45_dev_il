import React from 'react'
import { View } from 'react-native'
import { AdditionalKeyBoard } from './AdditionalKeyBoard'
import { MainKeyBoard } from './MainKeyBoard'

export const CommonKeyBord = () => {
	return (
		<View style={{ flexDirection: 'row' }}>
			<AdditionalKeyBoard />
			<MainKeyBoard />
		</View>
	)
}
