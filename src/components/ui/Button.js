import React, { useContext } from 'react'
import { Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { THERME } from '../../therme'
import { ScreenContext } from '../../context/screen/screenContext'

export const Button = ({ value, handler, style }) => {
	const { isLandscape } = useContext(ScreenContext)

	const btnStyles = [styles.btn]
	const txtStyles = [styles.btnText]

	if (style === 'secondary') {
		btnStyles.push(styles.btnSecondary)
		txtStyles.push(styles.btnTextSecondary)
	}

	if (style === 'accent') {
		btnStyles.push(styles.btnAccent)
	}

	if (style === 'double') {
		btnStyles.push(styles.btnDouble)
	}
	if (style === 'additional') {
		btnStyles.push(styles.btnAdditional)
		txtStyles.push(styles.btnTextAdditional)
	}
	return (
		<TouchableOpacity style={[btnStyles, { height: isLandscape ? smallBtn : btnWidth }]} onPress={handler}>
			<Text style={txtStyles}>{value}</Text>
		</TouchableOpacity>
	)
}
const screen = Dimensions.get('window')
const btnWidth = screen.width / 4 - THERME.BTN_MARGIN * 2
const smallBtn = screen.width / 7 - THERME.BTN_MARGIN * 2

const styles = StyleSheet.create({
	btnText: { color: THERME.TXT_WHITE, fontSize: 28, fontWeight: '500' },
	btn: {
		backgroundColor: THERME.DARK_GREY,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		margin: THERME.BTN_MARGIN,
		borderRadius: 100
	},
	btnSecondary: {
		backgroundColor: THERME.LIGHT_GREY,
		color: THERME.TXT_SECONDARY
	},
	btnTextSecondary: {
		color: THERME.TXT_SECONDARY
	},
	btnAccent: {
		backgroundColor: THERME.ACCENT_COLOR
	},
	btnDouble: {
		alignItems: 'flex-start',
		flex: 2,
		width: smallBtn * 2 + THERME.BTN_MARGIN,
		paddingLeft: smallBtn / 2 - THERME.BTN_MARGIN
	},
	btnAdditional: {
		backgroundColor: THERME.BKG_DARK
	},
	btnTextAdditional: { fontSize: 14 }
})
