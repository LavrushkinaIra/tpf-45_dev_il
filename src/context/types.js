export const NUMBER_CLICK = 'NUMBER_CLICK'
export const COMA_CLICK = 'COMA_CLICK'
export const SIGN_CLICK = 'SIGN_CLICK'
export const EQUALS_CLICK = 'EQUALS_CLICK'
export const INVERT_CLICK = 'INVERT_CLICK'
export const PERCENT_CLICK = 'PERCENT_CLICK'
export const RESET_CLICK = 'RESET_CLICK'
export const CHANGE_SCREEN = 'CHANGE_SCREEN'
