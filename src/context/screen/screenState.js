import React, { useReducer } from 'react'
import { Dimensions } from 'react-native'
import { ScreenContext } from './screenContext'
import { screenReducer } from './screenReducer'
import { CHANGE_SCREEN } from '../types'

export const ScreenState = ({ children }) => {
	const deviceWidth = Dimensions.get('window').width
	const deviceHeight = Dimensions.get('window').height

	const [state, dispatch] = useReducer(screenReducer, deviceWidth)

	const changeScreen = width => dispatch({ type: CHANGE_SCREEN, playloud: width })

	const isLandscape = deviceHeight < deviceWidth

	return (
		<ScreenContext.Provider value={{ changeScreen, screenWidth: state, deviceWidth, isLandscape }}>
			{children}
		</ScreenContext.Provider>
	)
}
