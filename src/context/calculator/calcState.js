import React, { useReducer } from 'react'
import { CalcContext } from './calcContext'
import { calcReducer } from './calcReducer'
import { NUMBER_CLICK, COMA_CLICK, SIGN_CLICK, EQUALS_CLICK, INVERT_CLICK, PERCENT_CLICK, RESET_CLICK } from '../types'

const zeroDivisionError = "Can't divide with 0"

export const CalcState = ({ children }) => {
	const iniatialState = {
		number: 0,
		result: 0,
		sign: ''
	}
	const [state, dispatch] = useReducer(calcReducer, iniatialState)

	const numberClickHandler = number => dispatch({ type: NUMBER_CLICK, number })

	const comaClickHandler = coma => dispatch({ type: COMA_CLICK, coma })

	const signClickHandler = value => dispatch({ type: SIGN_CLICK, value })

	const equalsClickHandler = () => dispatch({ type: EQUALS_CLICK })

	const invertClickHandler = () => dispatch({ type: INVERT_CLICK })

	const percentClickHandler = () => dispatch({ type: PERCENT_CLICK })

	const resetClickHandler = () => dispatch({ type: RESET_CLICK })

	const buttonClickHandler = btn => {
		btn === 'C' || state.result === zeroDivisionError
			? resetClickHandler()
			: btn === '+/-'
			? invertClickHandler()
			: btn === '%'
			? percentClickHandler()
			: btn === '='
			? equalsClickHandler()
			: btn === '/' || btn === 'x' || btn === '-' || btn === '+'
			? signClickHandler(btn)
			: btn === '.'
			? comaClickHandler(btn)
			: numberClickHandler(btn)
	}

	return (
		<CalcContext.Provider value={{ number: state.number, result: state.result, sign: state.sign, buttonClickHandler }}>
			{children}
		</CalcContext.Provider>
	)
}
