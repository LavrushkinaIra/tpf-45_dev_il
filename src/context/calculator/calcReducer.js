import { COMA_CLICK, NUMBER_CLICK, EQUALS_CLICK, SIGN_CLICK, INVERT_CLICK, PERCENT_CLICK, RESET_CLICK } from '../types'

const toLocaleString = num => String(num).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, '$1 ')

const removeSpaces = num => num.toString().replace(/\s/g, '')

const math = (a, b, sign) => (sign === '+' ? a + b : sign === '-' ? a - b : sign === 'x' ? a * b : a / b)

const zeroDivisionError = "Can't divide with 0"

const handlers = {
	[NUMBER_CLICK]: (state, { number }) => {
		if (removeSpaces(state.number).length < 16) {
			return {
				...state,
				number:
					removeSpaces(state.number) % 1 === 0 && !state.number.toString().includes('.')
						? toLocaleString(Number(removeSpaces(state.number + number)))
						: toLocaleString(state.number + number),
				result: !state.sign ? 0 : state.result
			}
		}
	},
	[COMA_CLICK]: (state, { coma }) => ({
		...state,
		number: !state.number.toString().includes('.') ? state.number + coma : state.number
	}),
	[SIGN_CLICK]: (state, { value }) => ({
		...state,
		sign: value,
		result: !state.number
			? state.result
			: !state.result
			? state.number
			: toLocaleString(math(state.sign, Number(removeSpaces(state.result)), Number(removeSpaces(state.number)))),
		number: 0
	}),
	[EQUALS_CLICK]: state => {
		if (state.sign && state.number) {
			return {
				...state,
				result:
					state.number === '0' && state.sign === '/'
						? zeroDivisionError
						: toLocaleString(math(Number(removeSpaces(state.result)), Number(removeSpaces(state.number)), state.sign)),
				sign: '',
				number: 0
			}
		}
	},
	[INVERT_CLICK]: state => ({
		...state,
		number: state.number ? toLocaleString(removeSpaces(state.number) * -1) : 0,
		result: state.result ? toLocaleString(removeSpaces(state.result) * -1) : 0,
		sign: ''
	}),
	[PERCENT_CLICK]: state => {
		let num = state.number ? parseFloat(removeSpaces(state.number)) : 0
		let res = state.result ? parseFloat(removeSpaces(state.result)) : 0
		return {
			...state,
			number: (num /= Math.pow(100, 1)),
			result: (res /= Math.pow(100, 1)),
			sign: ''
		}
	},
	[RESET_CLICK]: state => ({
		...state,
		sign: '',
		number: 0,
		result: 0
	}),
	DEFAULT: state => state
}

export const calcReducer = (state, action) => {
	const handler = handlers[action.type] || handlers.DEFAULT
	return handler(state, action)
}
